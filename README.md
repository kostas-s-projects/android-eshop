This is an android eshop(with pc parts) created with pure Java with Android Studio.With this app you can log in as an employee or as a regular user.As an employee you can edit orders,change the id,price,stock,category or the name of a product.As a regular user you can do more stuff,like put some parts on your wishlist,make an order,or you can even create your custom pc with your choice of parts.You can set the payment and delivery methods as you want.You can also register as a guest user without completing a registration form but in the end you have to manualy type your personal information about the payment method and delivery,unless you register with a username and complete a one-time form with your name,phone,adress etc.You can pay with a card or cash at the delivery.This particular project got some pc parts as well as some consoles,but you can add more products to your e-shop very very easily in the Initializer class inside the dao folder.You can also edit the employees and add/remove as many as you want.You can edit the parts in the same folder.
In order to run the app you MUST:
1)Update Android Studio to its latest version.
2)Update Java SDK,Gradle and Kotlin to its latest version(Kotlin is not used in this application)
Down below are some credential for some users:
1)Employees: 1)username: kostassot password:kostassot
             2)username: kwstaskwstas password:kwstaskwstas
             3)username: nikosnick password:nikosnick
2)Regular Users: 1)username: kostas password:kostas
                 2)username: kostas1 password:kostaskostas
                 3)username: nick password:nick           
